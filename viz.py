from dash import Dash, dcc, html
import plotly.express as px
import pandas as pd

app = Dash(__name__)

df = pd.read_csv('https://gist.githubusercontent.com/chriddyp/5d1ea79569ed194d432e56108a04d188/raw/a9f9e8076b837d541398e999dcbac2b2826a81f8/gdp-life-exp-2007.csv')

fig = px.scatter(df, x="gdp per capita", y="life expectancy",
                 size="population", color="continent", hover_name="country",
                 log_x=True, size_max=60)

## remove gridlines ##
fig.update_xaxes(showgrid=False, showspikes=True, spikemode='across')
fig.update_yaxes(showgrid=False)

## remove background color ##
fig.update_layout({
'paper_bgcolor': 'rgba(0, 0, 0, 0)',
'plot_bgcolor': 'rgba(0, 0, 0, 0)'},

# autosize=False,
    # width=500,
    # height=500,
    # margin=dict(
    #     l=50,
    #     r=50,
    #     b=100,
    #     t=100,
    #     pad=4)
    )


app.layout = html.Div([
    html.Div(
        className="app-header",
        children=[
            html.Div('Responsive Charts', className="app-header--title")
            ]
    ),
       html.Div(
            children=[
                dcc.Graph(
                    id='life-exp-vs-gdp',
                    figure=fig,
                    config={
                        'displayModeBar': False
                    }
                    # responsive='auto'
                )
        ]
    )
]#, style={'margin-top': '100px', 'margin-bottom': '100px', 'margin-left': '100px', 'margin-right': '100px'}
)

if __name__ == '__main__':
    app.run_server(debug=True)


    #  Looked into dash.html. Started playing with the margins.